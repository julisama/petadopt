package com.ramos.julian.petadopt.Buscar

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.support.v7.widget.Toolbar

import android.app.Activity.RESULT_OK
import android.support.design.widget.AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS
import android.support.design.widget.AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL
import android.support.design.widget.AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP
import android.util.Log
import com.ramos.julian.petadopt.R
import com.ramos.julian.petadopt.fragments.Buscar.MapFragment
import com.ramos.julian.petadopt.fragments.Buscar.ListFragment


class BuscarFragment : Fragment() {

    internal lateinit var simpleFrameLayout: FrameLayout
    internal lateinit var tabLayout: TabLayout
    private var MapList = 0
    internal lateinit var PetraceBuscartoolbar: Toolbar
    internal lateinit var MapListTabLayout: TabLayout
    internal lateinit var mAppBarLayout: AppBarLayout


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        super.onCreate(savedInstanceState)
        val root = inflater.inflate(R.layout.activity_buscar_fragment, container, false)
        simpleFrameLayout = root.findViewById(R.id.simpleFrameLayout)
        tabLayout = root.findViewById(R.id.MapListTabLayout)
        PetraceBuscartoolbar = root.findViewById(R.id.PetraceBuscartoolbar)
        MapListTabLayout = root.findViewById(R.id.MapListTabLayout)
        mAppBarLayout = root.findViewById(R.id.appbarlayout)
        val paramsP = PetraceBuscartoolbar.layoutParams as AppBarLayout.LayoutParams
        val paramsML = MapListTabLayout.layoutParams as AppBarLayout.LayoutParams


        var fragment: Fragment? = null
        fragment = MapFragment()
        val fm = fragmentManager
        val ft = fm!!.beginTransaction()
        ft.replace(R.id.simpleFrameLayout, fragment)
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        ft.commit()

        tabLayout.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                var fragment: Fragment? = null
                when (tab.position) {
                    0 -> {
                        fragment = MapFragment()
                        MapList = 0
                        paramsP.scrollFlags = 0
                        paramsML.scrollFlags = 0
                    }
                    1 -> {
                        fragment = ListFragment()
                        MapList = 1
                        paramsP.scrollFlags =
                            AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS or AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP
                        paramsML.scrollFlags =
                            AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS or AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP
                        val appBarLayoutParams = mAppBarLayout.layoutParams as CoordinatorLayout.LayoutParams

                        appBarLayoutParams.behavior = AppBarLayout.Behavior()
                        mAppBarLayout.layoutParams = appBarLayoutParams
                    }
                    else -> fragment = MapFragment()
                }
                val fm = fragmentManager
                val ft = fm!!.beginTransaction()
                ft.replace(R.id.simpleFrameLayout, fragment!!)
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                ft.commit()
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }

        })
        return root
    }
}
