package com.ramos.julian.petadopt.fragments.Buscar

import android.os.Bundle
import com.ramos.julian.petadopt.R

import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Rect
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast

import com.google.android.gms.maps.model.LatLng
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.ramos.julian.petadopt.models.GalleryCard

import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.Days
import org.joda.time.Hours
import org.joda.time.Minutes
import org.joda.time.Months
import org.joda.time.Seconds
import org.joda.time.Years

import java.text.NumberFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.ArrayList
import java.util.Collections
import java.util.Date
import java.util.Locale


class ListFragment : Fragment() {


    private var recyclerView: RecyclerView? = null
    private var adapter: ListAdapter? = null
    private var anuncioGaleríaObjectList: MutableList<GalleryCard>? = null
    //progressbar
    private var showLoading: LinearLayout? = null
    //refresh
    private var swipeRefreshLayout: SwipeRefreshLayout? = null

    internal var locationUser: Location? = null//ubicacion usuario
    private val mAuth = FirebaseAuth.getInstance()
    internal var uid = FirebaseAuth.getInstance().currentUser!!.uid


    @SuppressLint("MissingPermission")
    override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val rootView = inflater.inflate(R.layout.activity_list_fragment, container, false)

        showLoading = rootView.findViewById(R.id.linearLayout_progressBar)


        swipeRefreshLayout = rootView.findViewById(R.id.swipeRefreshLayout)
        swipeRefreshLayout!!.setColorSchemeResources(R.color.colorPrimary)
        swipeRefreshLayout!!.setProgressBackgroundColorSchemeResource(R.color.colorAccent)
        swipeRefreshLayout!!.setOnRefreshListener {
            RefreshList()
            swipeRefreshLayout!!.isRefreshing = false
            //swipeRefreshLayout.setEnabled(false);
        }



        recyclerView = rootView.findViewById(R.id.recycler_view)

        anuncioGaleríaObjectList = ArrayList()
        adapter = ListAdapter(activity!!, anuncioGaleríaObjectList!!)

        val mLayoutManager = GridLayoutManager(activity, 2)//columnas que quieres por filas
        recyclerView!!.layoutManager = mLayoutManager
        recyclerView!!.addItemDecoration(GridSpacingItemDecoration(2, dpToPx(10), true))
        recyclerView!!.itemAnimator = DefaultItemAnimator()
        recyclerView!!.adapter = adapter

        loadLocationUser()


        prepareAnuncios()

        return rootView

    }

    private fun loadLocationUser() {
        val rootRef = FirebaseDatabase.getInstance().reference
        val uidRef = rootRef.child("Usuarios").child(uid).child("Settings")
        val eventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val lat = dataSnapshot.child("lat").getValue(Double::class.java)
                val lng = dataSnapshot.child("lng").getValue(Double::class.java)
                //Log.i("localizacionUsuario", "Lat: "+String.valueOf(lat));
                //Log.i("localizacionUsuario", "Lng: "+String.valueOf(lng));

                locationUser = Location("locationUser")
                locationUser!!.latitude = lat!!
                locationUser!!.longitude = lng!!
                //Log.i("localizacionUsuario", String.valueOf(locationUser));


            }

            override fun onCancelled(databaseError: DatabaseError) {}
        }
        uidRef.addListenerForSingleValueEvent(eventListener)
    }

    //funcion crea anuncios llamando BBDD
    private fun prepareAnuncios() {
        if (!swipeRefreshLayout!!.isRefreshing) showLoading!!.visibility = View.VISIBLE
        val rootRef = FirebaseDatabase.getInstance().reference
        val adopcionesUbiRef = rootRef.child("Adopciones")
        val eventListener = object : ValueEventListener {


            @RequiresApi(api = Build.VERSION_CODES.N)
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                var ubicacion: LatLng
                var a: GalleryCard
                val mgr = activity!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
                if (ContextCompat.checkSelfPermission(
                        activity!!,
                        android.Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    Toast.makeText(activity, "Activa los permisos para detectar anuncios cerca", Toast.LENGTH_SHORT)
                        .show()
                    return
                }

                if (locationUser == null) {
                    locationUser = mgr.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
                }
                if (locationUser == null) {
                    locationUser =
                        getlocationIP()
                    Toast.makeText(
                        activity,
                        "Geolocalización estimada, para más exactitud activar GPS",
                        Toast.LENGTH_SHORT
                    ).show()
                    if (locationUser == null) {
                        Toast.makeText(activity, "Activa los Datos móviles por favor", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                for (ds in dataSnapshot.children) {
                    val id = ds.key
                    val name = ds.child("name").getValue(String::class.java)
                    val time = ds.child("fecha").getValue(String::class.java)
                    val descripcion = ds.child("descripción").getValue(String::class.java)
                    val Lat = ds.child("lat").getValue(Float::class.java)
                    val Long = ds.child("lng").getValue(Float::class.java)
                    ubicacion = LatLng(
                        java.lang.Float.parseFloat(Lat.toString()).toDouble(),
                        java.lang.Float.parseFloat(Long.toString()).toDouble()
                    )
                    val imageURL = ds.child("photoUrl").getValue(String::class.java)
                    var date: Date? = null
                    var dateLong: Long = 0
                    val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
                    try {
                        date = format.parse(time)
                        dateLong = date!!.time
                    } catch (e: ParseException) {
                        e.printStackTrace()
                    }

                    val DifTime = getTimeDifference(dateLong)
                    val locationPet = Location("locationPet")
                    locationPet.latitude = Lat!!.toDouble()
                    locationPet.longitude = Long!!.toDouble()
                    val distanceMeters = locationUser!!.distanceTo(locationPet)//calcula distancia
                    val distanceKm = distanceMeters / 1000
                    val distanceKmString =
                        String.format("%.02f", distanceKm)//pasar a string distancia i a 2 decimales



                    a = GalleryCard(id, "Adopciones", name, DifTime, distanceKmString, imageURL)
                    anuncioGaleríaObjectList!!.add(a)

                    //Ordena por su parametro km de los anuncios
                    Collections.sort(
                        anuncioGaleríaObjectList
                    ) { a1, a2 ->
                        var km1 = 0f
                        var km2 = 0f
                        try {
                            km1 = NumberFormat.getInstance().parse(a1.km)
                                .toFloat()//cambia el valor string en float per comparar
                            km2 = NumberFormat.getInstance().parse(a2.km).toFloat()
                        } catch (e: ParseException) {
                            e.printStackTrace()
                        }

                        if (km1 < km2)
                            -1// para ordenarlo
                        else if (km1 == km2)
                            0
                        else if (km1 > km2)
                            1
                        else
                            0
                    }

                }
                adapter!!.notifyDataSetChanged()
                swipeRefreshLayout!!.isRefreshing = false
                showLoading!!.visibility = View.INVISIBLE
            }


            override fun onCancelled(databaseError: DatabaseError) {

            }
        }
        adopcionesUbiRef.addListenerForSingleValueEvent(eventListener)

    }

    private fun getlocationIP(): Location {
        val locationUser = Location("locationUser")
        locationUser.latitude = 41.390205
        locationUser.longitude = 2.154007
        return locationUser
    }

    //coger fecha actual y fecha introducida y da la diferencia.
    private fun getTimeDifference(date: Long): String {
        val today = DateTime.now()
        val modified = DateTime(date, DateTimeZone.UTC)

        val years = Years.yearsBetween(modified, today).years
        val months = Months.monthsBetween(modified, today).months
        val days = Days.daysBetween(modified, today).days
        val hours = Hours.hoursBetween(modified, today).hours
        val minutes = Minutes.minutesBetween(modified, today).minutes
        val seconds = Seconds.secondsBetween(modified, today).seconds

        return if (years > 1) {
            String.format(Locale.getDefault(), getString(R.string.years), years)
        } else if (years > 0) {
            String.format(Locale.getDefault(), getString(R.string.year), years)
        } else if (months > 1) {
            String.format(Locale.getDefault(), getString(R.string.months), months)
        } else if (months > 0) {
            String.format(Locale.getDefault(), getString(R.string.month), months)
        } else if (days > 1) {
            String.format(Locale.getDefault(), getString(R.string.days), days)
        } else if (days > 0) {
            String.format(Locale.getDefault(), getString(R.string.day), days)
        } else if (hours > 1) {
            String.format(Locale.getDefault(), getString(R.string.hours), hours)
        } else if (hours > 0) {
            String.format(Locale.getDefault(), getString(R.string.hour), hours)
        } else if (minutes > 1) {
            String.format(Locale.getDefault(), getString(R.string.minutes), minutes)
        } else if (minutes > 0) {
            String.format(Locale.getDefault(), getString(R.string.minute), minutes)
        } else if (seconds > 1) {
            String.format(Locale.getDefault(), getString(R.string.seconds), seconds)
        } else {
            String.format(Locale.getDefault(), getString(R.string.second), seconds)
        }
    }

    inner class GridSpacingItemDecoration(
        private val spanCount: Int,
        private val spacing: Int,
        private val includeEdge: Boolean
    ) : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
            val position = parent.getChildAdapterPosition(view) // item position
            val column = position % spanCount // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing
                }
                outRect.bottom = spacing // item bottom
            } else {
                outRect.left = column * spacing / spanCount // column * ((1f / spanCount) * spacing)
                outRect.right =
                    spacing - (column + 1) * spacing / spanCount // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing // item top
                }
            }
        }
    }


    fun RefreshList() {
        Type_Ads = "Adopciones"
        if (anuncioGaleríaObjectList != null) anuncioGaleríaObjectList!!.clear()
        prepareAnuncios()

    }

    private fun dpToPx(dp: Int): Int {
        val r = resources
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), r.displayMetrics))
    }

    companion object {

        var Type_Ads = "Adopciones"
    }


}// Required empty public constructor