package com.ramos.julian.petadopt.fragments.Profile

import com.squareup.picasso.Picasso

import java.io.File
import java.util.List
import de.hdodenhof.circleimageview.CircleImageView
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.ramos.julian.petadopt.R
import android.annotation.SuppressLint
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.support.annotation.NonNull
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast

import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.activity_login.*
import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage


class EditProfileActivity : AppCompatActivity(), View.OnClickListener {

    private val mAuth = FirebaseAuth.getInstance()
    internal var uid = FirebaseAuth.getInstance().currentUser!!.uid
    private val PHOTO_REQUEST = 9002
    private val REQUEST_READ_PERMISSION = 9003
    private val items = arrayOf("Cámara", "Galería")
    private var photoUri: Uri? = null
    private var mStorageReference: StorageReference? = null
    internal var uidRef = FirebaseDatabase.getInstance().reference.child("Usuarios").child(uid)
    internal var img: String? = null
    internal var photoUrl: String? = null


    override fun onClick(v: View?) {
        when (v!!.getId()) {
            R.id.Back -> onBackPressed()
            R.id.btn_accept -> apply()
            R.id.btn_cancel -> cancel()
            R.id.user_image -> uploadImage()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        mStorageReference = FirebaseStorage.getInstance().reference
        val rootRef = FirebaseDatabase.getInstance().reference
        val uidRef = rootRef.child("Usuarios").child(uid)
        val eventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val name = dataSnapshot.child("name").getValue(String::class.java)
                val email = dataSnapshot.child("email").getValue(String::class.java)
                img = dataSnapshot.child("image").getValue(String::class.java)
                //System.out.println(name);
                txtName.text = name
                txtEmail.text = email
                if (img != "default" && img != null) {
                    //picasso
                    Picasso.get()
                        .load(img)
                        .resize(250, 250)
                        .centerCrop()
                        .into(user_image)
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        }
        uidRef.addListenerForSingleValueEvent(eventListener)
    }


    private fun uploadImage() {
        requestPermission()
    }

    private fun requestPermission() {
        //storage
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    this@EditProfileActivity,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this@EditProfileActivity,
                    arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),
                    REQUEST_READ_PERMISSION
                )
            } else {
                openFilePicker()
            }
        } else {
            openFilePicker()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            //storage
            REQUEST_READ_PERMISSION -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    openFilePicker()

                } else {
                    Toast.makeText(
                        this@EditProfileActivity,
                        "Es necesario aceptar los permisos, gracias.",
                        Toast.LENGTH_LONG
                    )
                        .show()
                }
            }
        }
    }

    private fun openFilePicker() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Elige una opción")
        builder.setItems(items) { dialogInterface, i ->
            if (items[i] == "Cámara") {
                EasyImage.openCamera(this@EditProfileActivity, PHOTO_REQUEST)
            } else if (items[i] == "Galería") {
                EasyImage.openGallery(this@EditProfileActivity, PHOTO_REQUEST)
            }
        }

        val dialog = builder.create()
        dialog.show()
    }

    private fun updateImage() {
        val onlineStoragePhotoRef =
            mStorageReference!!.child("Usuarios").child(uid).child(photoUri!!.getLastPathSegment()!!)
        onlineStoragePhotoRef.putFile(photoUri!!).addOnSuccessListener { taskSnapshot ->
            photoUrl = taskSnapshot.downloadUrl!!.toString()
            uidRef.child("image").setValue(photoUrl)
            Toast.makeText(applicationContext, "Imagen de perfil actualizada.", Toast.LENGTH_SHORT).show()
            val data = Intent()
            setResult(Activity.RESULT_OK, data)
            onBackPressed()
        }.addOnFailureListener(OnFailureListener {
            Toast.makeText(applicationContext, "Error, por favor vuelve a intentarlo.", Toast.LENGTH_SHORT).show()
            return@OnFailureListener
        })
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, object : DefaultCallback() {
            @SuppressLint("ResourceType")
            override fun onImagesPicked(
                imageFiles: kotlin.collections.List<File>,
                source: EasyImage.ImageSource,
                type: Int
            ) {
                when (type) {
                    PHOTO_REQUEST -> {
                        Glide.with(this@EditProfileActivity)
                            .load(imageFiles[0])
                            .centerCrop()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(user_image)
                        photoUri = Uri.fromFile(imageFiles[0])
                    }
                }// RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(1000, 1000);
                //photo.setLayoutParams(layoutParams);
            }


        })
    }

    private fun deleteActualImage() {
        //borra anuncio y foto
        val rootRef = FirebaseDatabase.getInstance().reference
        val uidRef = rootRef.child("Usuarios").child(uid)
        val mFirebaseStorage = FirebaseStorage.getInstance().reference.storage

        val eventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val photoUrl2 = dataSnapshot.child("image").getValue(String::class.java)
                if (img == "default" || img == null) return
                val img = mFirebaseStorage.getReferenceFromUrl(photoUrl2!!)
                //borra la imagen
                img.delete().addOnSuccessListener { }.addOnFailureListener {
                    Toast.makeText(
                        this@EditProfileActivity,
                        "Error al intentar actualizar al imagen",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                Toast.makeText(this@EditProfileActivity, "Fallo de conexión", Toast.LENGTH_SHORT).show()
            }
        }
        uidRef.addListenerForSingleValueEvent(eventListener)

    }

    private fun apply() {
        if (photoUri != null) {
            deleteActualImage()
            updateImage()
        }
        onBackPressed()
    }

    private fun cancel() {
        onBackPressed()
    }

    override fun onBackPressed() {
        this.finish()
        overridePendingTransition(R.animator.enter_reverse, R.animator.exit_reverse)
    }
}




