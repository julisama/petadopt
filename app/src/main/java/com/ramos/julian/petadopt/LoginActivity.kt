package com.ramos.julian.petadopt

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.*
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private var auth: FirebaseAuth? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        //Get Firebase auth instance
        auth = FirebaseAuth.getInstance()

        if (auth!!.currentUser != null) { // si ya se ha autentificado pasar directamente a usuario
            startActivity(Intent(this@LoginActivity, MainActivity::class.java))
            finish()
        }

        setContentView(R.layout.activity_login)
        pass_forgotten.setOnClickListener {
            val intent = Intent(this@LoginActivity, ResetPasswordActivity::class.java)
            startActivity(intent)
            //Toast.makeText(LoginActivity.this, R.string.it_doesnt_work, Toast.LENGTH_SHORT).show();
        }
        //btnReset = (Button) findViewById(R.id.btn_reset_password);

        //Get Firebase auth instance
        auth = FirebaseAuth.getInstance()

        login_registrar.setOnClickListener {
            startActivity(
                Intent(
                    this@LoginActivity,
                    RegistroActivity::class.java
                )
            )
        }

        login_entrar.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                val email = login_email.text.toString()
                val password = login_pass.text.toString()

                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(applicationContext, "¡Pon tu email!", Toast.LENGTH_SHORT).show()
                    return
                }

                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(applicationContext, "¡Pon tu contraseña!", Toast.LENGTH_SHORT).show()
                    return
                }

                progressBar.visibility = View.VISIBLE

                //authenticate user
                auth!!.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(
                        this@LoginActivity
                    ) { task ->
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        progressBar.visibility = View.GONE
                        if (!task.isSuccessful) {
                            // there was an error
                            /* if (password.length() < 6) {
                                                        inputPassword.setError(getString(R.string.minimum_password));
                                                    } else {*/
                            Toast.makeText(
                                this@LoginActivity,
                                "Autentificación fallida, revisa tu email y contraseña...",
                                Toast.LENGTH_LONG
                            ).show()
                            /*  } */
                        } else {
                            val intent = Intent(this@LoginActivity, MainActivity::class.java)
                            startActivity(intent)
                            finish()
                        }
                    }
            }
        })
    }
}

