package com.ramos.julian.petadopt.fragments.Profile

import android.app.Activity.RESULT_OK
import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.ramos.julian.petadopt.LoginActivity
import com.ramos.julian.petadopt.R
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView


class ProfileFragment : Fragment() {


    private val mAuthListener: FirebaseAuth.AuthStateListener? = null

    private var btnLogOut: Button? = null
    private var btnmisanuncios: Button? = null
    private var btnajustes: Button? = null
    private var btncontacto: Button? = null
    private var btn_myLocation: Button? = null
    private val CAMERA_REQUEST_CODE = 0
    private val progressDialog: ProgressDialog? = null
    // private StorageReference mStorage;
//private ImageView imageProfile;
    private var textName: TextView? = null
    private var textEmail: TextView? = null
    internal lateinit var user_image: CircleImageView
    internal var img: String? = null


    private val mAuth = FirebaseAuth.getInstance()
    internal var uid = FirebaseAuth.getInstance().currentUser!!.uid
    //edit profile
    private var btn_edit: FloatingActionButton? = null

    //interfaz para poder hacer finish en fragment i eliminar la actividad completa
    interface finishMainActivity {
        fun finishMainActivity()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_profile, container, false)
        textName = view.findViewById(R.id.txtName)
        textEmail = view.findViewById(R.id.txtEmail)

        //GET VALUES FROM SHAREDPREFERENCER "USER"---------------------
        val preferences = this.activity!!.getSharedPreferences("user", Context.MODE_PRIVATE)
        textName!!.text = preferences.getString("userName", "cargando...")
        textEmail!!.text = preferences.getString("userEmail", "cargando...")
        //-------------------------------

        user_image = view.findViewById(R.id.user_image)

        btn_edit = view.findViewById(R.id.edit_profile)
        btn_edit!!.setOnClickListener {
            val requestCode = 0

            val intent = Intent(activity, EditProfileActivity::class.java)
            startActivityForResult(intent, requestCode)


        }


        loadProfileInfo()




        btnLogOut = view.findViewById(R.id.btn_logout)
        btnLogOut!!.setOnClickListener {
            if (mAuth.currentUser != null) showAlert(
                R.string.user_logout_title,
                String.format(getString(R.string.user_logout))
            )
        }


        btn_myLocation = view.findViewById(R.id.btn_myLocation)
        btn_myLocation!!.setOnClickListener {
            val intent = Intent(activity, MyLocationActivity::class.java)
            startActivity(intent)
        }


        btnajustes = view.findViewById(R.id.btn_ajustes)
        btnajustes!!.setOnClickListener {
            //Toast.makeText(getActivity(), R.string.it_doesnt_work, Toast.LENGTH_SHORT).show();

            val intent = Intent(activity, SettingsActivity::class.java)
            startActivity(intent)
        }




        btncontacto = view.findViewById(R.id.btn_contacto)
        btncontacto!!.setOnClickListener {
            openURLinBrowserContact()
            /*
        Intent intent = new Intent(getActivity(), ContactActivity.class);
        startActivity(intent);*/
        }
        return view
    }

    private fun logout() {
        mAuth.signOut()
        val intent = Intent(activity, LoginActivity::class.java)
        startActivity(intent)
        val finish = this as finishMainActivity
        finish.finishMainActivity()
    }

    private fun showAlert(title: Int, message: String) {
        val alertDialogBuilder = android.support.v7.app.AlertDialog.Builder(activity!!, R.style.MyDialogTheme)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(R.string.logout__button, DialogInterface.OnClickListener { dialog, which ->
                dialog.dismiss()
                logout()
            })
            .setNegativeButton(R.string.logout__button_cancel,
                DialogInterface.OnClickListener { dialog, which -> dialog.dismiss() })
        val alertDialog = alertDialogBuilder.create()
        alertDialog.show()
    }


    fun openURLinBrowserContact() {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://mail.google.com/mail/u/0/"))
        startActivity(browserIntent)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 0 && resultCode == RESULT_OK) {
            loadProfileInfo()
        }
    }

    private fun loadProfileInfo() {
        val rootRef = FirebaseDatabase.getInstance().reference
        val uidRef = rootRef.child("Usuarios").child(uid)
        val eventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val name = dataSnapshot.child("name").getValue(String::class.java)
                val email = dataSnapshot.child("email").getValue(String::class.java)
                img = dataSnapshot.child("image").getValue(String::class.java)

                //System.out.println(name);
                textName!!.text = name
                textEmail!!.text = email
                //Name_nav.setText(name);
                if (img != "default" && img != null) {
                    //picasso
                    Picasso.get()
                        .load(img)
                        .resize(250, 250)
                        .centerCrop()
                        .into(user_image)
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        }
        uidRef.addListenerForSingleValueEvent(eventListener)
    }
}
