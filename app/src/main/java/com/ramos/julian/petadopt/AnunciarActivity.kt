package com.ramos.julian.petadopt

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.text.TextUtils
import android.view.View
import android.widget.*

import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.ui.PlacePicker
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.ramos.julian.petadopt.models.Pet
import kotlinx.android.synthetic.main.activity_anunciar.*

import java.io.File
import java.text.SimpleDateFormat
import java.util.Date

import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage

class AnunciarActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var userId: String
    private var fecha: String? = null
    private var direccion: String? = null
    private var Sex: String? = null
    private var type_animal: String? = null
    //place api
    internal var Lat = 0f
    internal var Lng = 0f
    internal lateinit var LatLng: com.google.android.gms.maps.model.LatLng
    private val MY_PERMISSION_FINE_LOCATION = 101
    private val PLACE_PICKER_REQUEST = 1
    //--------------------------------------------------------
    //photo
    internal lateinit var background1: RelativeLayout

    private var mDatabaseReference: DatabaseReference? = null
    private var mStorageReference: StorageReference? = null
    private var photoUri: Uri? = null
    private var progressBar: ProgressDialog? = null
    private var photoUrl: String? = null
    private val items = arrayOf("Cámara", "Galería")
    private val PHOTO_REQUEST = 9002
    private val REQUEST_READ_PERMISSION = 9003
    //save image
    internal lateinit var tipoAnuncioRef: DatabaseReference


    //datas to save
    internal lateinit var TipoAnuncio: String
    internal lateinit var key: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_anunciar)

        val intent = intent
        TipoAnuncio = "Adopciones"
        btGetPlace.setOnClickListener {
            val builder = PlacePicker.IntentBuilder()
            try {
                val intent = builder.build(this@AnunciarActivity)
                startActivityForResult(intent, PLACE_PICKER_REQUEST)
            } catch (e: GooglePlayServicesRepairableException) {
                e.printStackTrace()
            } catch (e: GooglePlayServicesNotAvailableException) {
                e.printStackTrace()
            }
        }
        progressBar = ProgressDialog(this@AnunciarActivity)
        mStorageReference = FirebaseStorage.getInstance().reference
        mDatabaseReference = FirebaseDatabase.getInstance().reference
        save!!.setOnClickListener(View.OnClickListener {
            var nameText = ""

            var descripciónText = ""
            var razaText = ""
            val edadText = ""
            var ojosText = ""
            var peloText = ""
            var Sex = ""
            var telephone_value = 0
            var collar = false
            var chip = false
            var latFloat = 0f
            var longFloat = 0f
            var type_animal = ""

            if (photoUri == null) {
                Toast.makeText(applicationContext, "¡Pon una foto!", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            if (name!!.text.toString() != null) {
                nameText = name!!.text.toString()

                if (TextUtils.isEmpty(nameText)) {
                    Toast.makeText(applicationContext, "¡Pon el nombre!", Toast.LENGTH_SHORT).show()
                    return@OnClickListener
                }
            }

            if (name!!.text.toString() != null) {
                nameText = name!!.text.toString()

                if (TextUtils.isEmpty(nameText)) {
                    Toast.makeText(applicationContext, "¡Pon el nombre!", Toast.LENGTH_SHORT).show()
                    return@OnClickListener
                }
            }
            if (raza!!.text.toString() != null) {
                razaText = raza!!.text.toString()
            }

            if (male_pet.alpha == 1.0f)
                Sex = "M"
            else if (female_pet.alpha == 1.0f)
                Sex = "F"
            else if (unknown_pet.alpha == 1.0f)
                Sex = "U"
            else {
                Toast.makeText(applicationContext, "¡Pon el sexo!", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }

            if (ojos!!.text.toString() != null) {
                ojosText = ojos.text.toString()
                if (TextUtils.isEmpty(ojosText)) {
                    Toast.makeText(applicationContext, "¡Pon un color de ojos!", Toast.LENGTH_SHORT).show()
                    return@OnClickListener
                }
            }
            if (pelo!!.text.toString() != null) {
                peloText = pelo.text.toString()
                if (TextUtils.isEmpty(peloText)) {
                    Toast.makeText(applicationContext, "¡Pon una color de pelo!", Toast.LENGTH_SHORT).show()
                    return@OnClickListener
                }
            }

            if (cb_collar.isChecked) collar = true
            if (cb_chip.isChecked) chip = true

            if (Lat != 0f && Lng != 0f) {
                latFloat = Lat
                longFloat = Lng
            } else {
                Toast.makeText(applicationContext, "¡Pon una ubicación!", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            if (description.text.toString() != null) {
                descripciónText = description!!.text.toString()
            }
            if (telephone.text.toString() != null) { //si es empty poner
                if (!telephone.text.toString().isEmpty()) telephone_value =
                    Integer.parseInt(telephone!!.text.toString())//coge int de edit text
            }




            progressBar!!.setMessage("Guardando...")
            progressBar!!.show()

            val finalNameText = nameText
            val finalDescripciónText = descripciónText
            val finalRazaText = razaText
            val finalOjosText = ojosText
            val finalPeloText = peloText
            val finalLatText = latFloat
            val finalLongText = longFloat


            userId = FirebaseAuth.getInstance().currentUser!!.uid
            fecha = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").format(Date())
            //Get ID(key) of Ad
            tipoAnuncioRef = mDatabaseReference!!.child(TipoAnuncio)
            key = tipoAnuncioRef.push().key //This is the value of your key

            //sube imagen
            val onlineStoragePhotoRef =
                mStorageReference!!.child(TipoAnuncio).child(key).child(photoUri!!.lastPathSegment!!)
            linearLayout_progressBar.visibility = View.VISIBLE
            onlineStoragePhotoRef.putFile(photoUri!!).addOnSuccessListener { taskSnapshot ->
                progressBar!!.setMessage("Success")
                photoUrl = taskSnapshot.downloadUrl!!.toString()
                val fichaPet = Pet(
                    finalNameText,
                    finalDescripciónText,
                    finalRazaText,
                    finalPeloText,
                    finalOjosText,
                    photoUrl,
                    userId,
                    finalLatText,
                    finalLongText,
                    fecha,
                    direccion,
                    Sex,
                    collar,
                    chip,
                    telephone_value,
                    type_animal,
                    TipoAnuncio
                )
                tipoAnuncioRef.child(key).setValue(fichaPet)
                mDatabaseReference!!.child("Usuarios").child(userId!!).child("Ads").child(key).setValue(TipoAnuncio)
                CallPostAdPublicationActivity()
                //AnunciarActivity.instance.finish()
                linearLayout_progressBar.visibility = View.INVISIBLE
                //cierra pantalla
                finish()
            }.addOnFailureListener(OnFailureListener {
                Toast.makeText(applicationContext, "Error, por favor vuelve a intentarlo.", Toast.LENGTH_SHORT).show()
                return@OnFailureListener
            })

            progressBar!!.dismiss()
        })

        this.setFinishOnTouchOutside(true)
    }

    private fun CallPostAdPublicationActivity() {
        val intent = Intent(this@AnunciarActivity, PostAdPublicationActivity::class.java)
        intent.putExtra("TipoAnuncio", TipoAnuncio)
        intent.putExtra("IdAnuncio", key)
        startActivity(intent)
    }

    fun ImageOnClick(view: View) {
        requestPermission()
    }

    private fun requestPermission() {
        //api place
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), MY_PERMISSION_FINE_LOCATION)
            }
        }
        //storage
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),
                    REQUEST_READ_PERMISSION
                )
            } else {
                openFilePicker()
            }
        } else {
            openFilePicker()
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            //api place
            MY_PERMISSION_FINE_LOCATION -> if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(applicationContext, "Es necesario aceptar los permisos, gracias.", Toast.LENGTH_LONG)
                    .show()
                finish()
            }
            //storage
            REQUEST_READ_PERMISSION -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    openFilePicker()

                } else {
                    Toast.makeText(this, "No se puede acceder", Toast.LENGTH_LONG).show()
                }
            }
        }
    }


    private fun openFilePicker() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Elige una opción")
        builder.setItems(items) { dialogInterface, i ->
            if (items[i] == "Cámara") {
                EasyImage.openCamera(this, PHOTO_REQUEST)
            } else if (items[i] == "Galería") {
                EasyImage.openGallery(this, PHOTO_REQUEST)
            }
        }

        val dialog = builder.create()
        dialog.show()
    }


    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, object : DefaultCallback() {
            @SuppressLint("ResourceType")
            override fun onImagesPicked(imageFiles: List<File>, source: EasyImage.ImageSource, type: Int) {
                when (type) {
                    PHOTO_REQUEST -> {
                        Glide.with(this@AnunciarActivity)
                            .load(imageFiles[0])
                            .centerCrop()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(photo)
                        photoUri = Uri.fromFile(imageFiles[0])

                        //bad full image
                        val layoutParams = RelativeLayout.LayoutParams(1000, 1000)
                        photo!!.setLayoutParams(layoutParams)
                    }
                }//BitmapDrawable drawable = (BitmapDrawable) photo.getDrawable();
                //background1.setBackground(drawable);

            }


        })
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                val place = PlacePicker.getPlace(this, data!!)
                tvPlaceName.text = place.name
                tvPlaceAddress.text = place.address
                direccion = place.address as String
                LatLng = place.latLng
                Lat = LatLng.latitude.toFloat()
                Lng = LatLng.longitude.toFloat()
                //Log.i("placeapi", "long y lat "+Lat+" "+Lng);
            }
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.male_pet -> {
                male_pet.alpha = 1.0f
                female_pet.alpha = 0.5f
                unknown_pet.alpha = 0.5f
            }
            R.id.female_pet -> {
                male_pet.alpha = 0.5f
                female_pet.alpha = 1.0f
                unknown_pet.alpha = 0.5f
            }
            R.id.unknown_pet -> {
                male_pet.alpha = 0.5f
                female_pet.alpha = 0.5f
                unknown_pet.alpha = 1.0f
            }
        }


    }
}





