package com.ramos.julian.petadopt.fragments.Profile

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.Switch

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.ramos.julian.petadopt.R


class SettingsActivity : AppCompatActivity(), View.OnClickListener {

    private var switchAlerts: Switch? = null
    private var switchAlertAdopciones: Switch? = null
    private var layouNotificacions: LinearLayout? = null
    private var uid: String? = null
    internal lateinit var mAuth: FirebaseAuth
    internal lateinit var rootRef: DatabaseReference
    internal lateinit var uidRef: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        switchAlerts = findViewById(R.id.switchAlerts)
        switchAlertAdopciones = findViewById(R.id.switchAlertAdopciones)
        layouNotificacions = findViewById(R.id.layouNotificacions)
        //cargar datos
        mAuth = FirebaseAuth.getInstance()
        uid = FirebaseAuth.getInstance().currentUser!!.uid
        rootRef = FirebaseDatabase.getInstance().reference
        uidRef = rootRef.child("Usuarios").child(uid!!)
        val eventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val alerts = dataSnapshot.child("Settings").child("alerts").getValue(Boolean::class.java)
                val alertAdopciones =
                    dataSnapshot.child("Settings").child("alertAdopciones").getValue(Boolean::class.java)
                switchAlerts!!.isChecked = alerts!!
                switchAlertAdopciones!!.isChecked = alertAdopciones!!
                switchAlertsOnCLick()


            }

            override fun onCancelled(databaseError: DatabaseError) {}
        }
        uidRef.addListenerForSingleValueEvent(eventListener)

    }


    override fun onClick(v: View) {
        when (v.id) {
            R.id.Back -> onBackPressed()
            R.id.switchAlerts -> switchAlertsOnCLick()
            R.id.switchAlertAdopciones -> if (!switchAlertAdopciones!!.isChecked) {
                rootRef.child("Usuarios").child(uid!!).child("Settings").child("alertAdopciones").setValue(false)
            } else if (switchAlertAdopciones!!.isChecked) {
                rootRef.child("Usuarios").child(uid!!).child("Settings").child("alertAdopciones").setValue(true)
            }
        }
    }

    private fun switchAlertsOnCLick() {
        if (!switchAlerts!!.isChecked) {
            layouNotificacions!!.visibility = View.INVISIBLE
            //si no notis, desactiva todas
            rootRef.child("Usuarios").child(uid!!).child("Settings").child("alerts").setValue(false)
            rootRef.child("Usuarios").child(uid!!).child("Settings").child("alertAdopciones").setValue(false)


            switchAlertAdopciones!!.isChecked = false
        } else if (switchAlerts!!.isChecked) {
            layouNotificacions!!.visibility = View.VISIBLE
            rootRef.child("Usuarios").child(uid!!).child("Settings").child("alerts").setValue(true)
        }
    }

    override fun onBackPressed() {
        this.finish()
        overridePendingTransition(R.animator.enter_reverse, R.animator.exit_reverse)
    }

}

