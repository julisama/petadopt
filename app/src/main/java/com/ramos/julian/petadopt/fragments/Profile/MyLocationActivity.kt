package com.ramos.julian.petadopt.fragments.Profile

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.SeekBar
import android.widget.TextView
import android.widget.Toast

import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.ui.PlacePicker
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.Circle
import com.google.android.gms.maps.model.CircleOptions
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.ramos.julian.petadopt.R

class MyLocationActivity : AppCompatActivity(), View.OnClickListener, OnMapReadyCallback {

    private var uid: String? = null
    private var direccion: String? = null
    internal lateinit var mAuth: FirebaseAuth
    internal lateinit var rootRef: DatabaseReference
    internal lateinit var uidRef: DatabaseReference
    private var lat: Float = 0.toFloat()
    private var lng: Float = 0.toFloat()
    private var radius: Int = 0
    private var directionTv: TextView? = null
    //mapa
    private var mMap: GoogleMap? = null
    private var getPlaceButton: Button? = null
    internal lateinit var LatLng: com.google.android.gms.maps.model.LatLng
    //seekbar
    private var radiusSB: SeekBar? = null
    private var numberSB: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_location)

        directionTv = findViewById(R.id.direccionUser)
        numberSB = findViewById(R.id.numberSB)
        radiusSB = findViewById(R.id.radiusSB)
        //map
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)
        //cargar datos
        mAuth = FirebaseAuth.getInstance()
        uid = FirebaseAuth.getInstance().currentUser!!.uid
        rootRef = FirebaseDatabase.getInstance().reference
        uidRef = rootRef.child("Usuarios").child(uid!!)
        val eventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                lat = dataSnapshot.child("Settings").child("lat").getValue(Float::class.java)!!
                lng = dataSnapshot.child("Settings").child("lng").getValue(Float::class.java)!!
                radius = dataSnapshot.child("Settings").child("radius").getValue(Int::class.java)!!
                direccion = dataSnapshot.child("Settings").child("address").getValue(String::class.java)
                directionTv!!.text = "Tu dirección es " + direccion!!
                radiusSB!!.progress = radius
                numberSB!!.text = "Radio de $radius Km"

                centerCamera()
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        }
        uidRef.addListenerForSingleValueEvent(eventListener)

        getPlaceButton = findViewById(R.id.btGetPlace) as Button
        getPlaceButton!!.setOnClickListener {
            val builder = PlacePicker.IntentBuilder()
            try {
                val intent = builder.build(this@MyLocationActivity)
                startActivityForResult(intent, PLACE_PICKER_REQUEST)
            } catch (e: GooglePlayServicesRepairableException) {
                e.printStackTrace()
            } catch (e: GooglePlayServicesNotAvailableException) {
                e.printStackTrace()
            }
        }
        //SeekBar

        radiusSB!!.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {

                radius = i

                numberSB!!.text = "Radio de $radius Km"
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {

            }

            override//quan para de moure fer que cercle varii
            fun onStopTrackingTouch(seekBar: SeekBar) {
                mMap!!.clear()//si no no borra circulo anterior
                centerCamera()

            }
        })

    }


    //MAPA
    private fun centerCamera() {
        if (lat != 0f && lng != 0f && mMap != null && radius != 0) {
            val location = LatLng(
                java.lang.Float.parseFloat(lat.toString()).toDouble(),
                java.lang.Float.parseFloat(lng.toString()).toDouble()
            )
            mMap!!.moveCamera(CameraUpdateFactory.newLatLng(location))
            var scale = 11.0f
            if (radius < 3)
                scale = 13.0f
            else if (radius < 10)
                scale = 11.0f
            else if (radius < 19)
                scale = 10.0f
            else if (radius < 40)
                scale = 9.0f
            else if (radius < 50)
                scale = 8.5f
            else if (radius < 80)
                scale = 8.0f
            else if (radius < 250)
                scale = 6.0f
            else if (radius <= 300) scale = 5.5f

            mMap!!.animateCamera(
                CameraUpdateFactory.newLatLngZoom(
                    location,
                    scale
                )
            )//Aroximitat, faig escala (com menys, més lluny)

            val circleOptions = CircleOptions()
                .center(
                    LatLng(
                        java.lang.Float.parseFloat(lat.toString()).toDouble(),
                        java.lang.Float.parseFloat(lng.toString()).toDouble()
                    )
                )
                .radius((radius * 1000).toDouble())//metres
                .strokeWidth(0f)
                .fillColor(Color.argb(35, 0, 0, 0))

            val circle = mMap!!.addCircle(circleOptions)
            circle.center = location
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.Back -> onBackPressed()
        }
    }

    override fun onBackPressed() {
        this.finish()
        overridePendingTransition(R.animator.enter_reverse, R.animator.exit_reverse)
    }

    fun cancel(view: View) {
        finish()
    }

    fun apply(view: View) {

        rootRef.child("Usuarios").child(uid!!).child("Settings").child("lat").setValue(lat)
        rootRef.child("Usuarios").child(uid!!).child("Settings").child("lng").setValue(lng)
        rootRef.child("Usuarios").child(uid!!).child("Settings").child("address").setValue(direccion)
        rootRef.child("Usuarios").child(uid!!).child("Settings").child("radius").setValue(radius)

        Toast.makeText(this, "Nueva ubicación actualizada", Toast.LENGTH_SHORT).show()
        finish()
    }


    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        //al retornar intent de la ubicación
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                val place = PlacePicker.getPlace(this, data!!)
                //placeNameText.setText(place.getName());
                //placeAddressText.setText(place.getAddress());
                direccion = place.address as String
                LatLng = place.latLng
                lat = LatLng.latitude.toFloat()
                lng = LatLng.longitude.toFloat()
                directionTv!!.text = "Tu dirección es " + direccion!!
                centerCamera()


                //Log.i("placeapi", "long y lat "+Lat+" "+Lng);

            }
        }
    }

    companion object {
        private val PLACE_PICKER_REQUEST = 1
    }
}
