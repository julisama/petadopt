package com.ramos.julian.petadopt.fragments.Buscar

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.iid.FirebaseInstanceId

import java.util.ArrayList
import java.util.HashMap

import android.app.Activity.RESULT_OK
import com.ramos.julian.petadopt.DataPetActivity
import com.ramos.julian.petadopt.R
import kotlinx.android.synthetic.main.activity_map_fragment.*


class MapFragment : Fragment() {
    //drawable problem line90 null reference
    internal var contextThis: Context? = null

    //progressbar
    private var showLoading: LinearLayout? = null

    private var googleMap: GoogleMap? = null
    internal var markerPoints: ArrayList<LatLng>? = null

    internal var extraData = HashMap<Marker, String>()
    internal var extraData2 = HashMap<Marker, String>()
    internal var extraData3 = HashMap<Marker, String>()

    internal lateinit var mMapView: MapView

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        contextThis = context

    }

    private fun bitmapDescriptorFromVector(context: Context?, vectorResId: Int): BitmapDescriptor {
        var context = context
        if (context == null) {
            if (contextThis != null) context = contextThis
        }
        val vectorDrawable = ContextCompat.getDrawable(context!!, vectorResId)
        vectorDrawable!!.setBounds(0, 0, vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight)
        val bitmap =
            Bitmap.createBitmap(vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.activity_map_fragment, container, false)

        //refresh token
        val token = FirebaseInstanceId.getInstance().token
        val currentFirebaseUser = FirebaseAuth.getInstance().currentUser
        val ref = FirebaseDatabase.getInstance().reference.child("Usuarios/" + currentFirebaseUser!!.uid + "/token")
        ref.setValue(token)


        showLoading = rootView.findViewById(R.id.linearLayout_progressBar)

        mMapView = rootView.findViewById(R.id.mapView) as MapView

        mMapView.onCreate(savedInstanceState)

        mMapView.onResume()

        try {
            MapsInitializer.initialize(activity!!.applicationContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        mMapView.getMapAsync { mMap ->
            googleMap = mMap
            if (checkLocationPermission()) {
                if (ContextCompat.checkSelfPermission(
                        activity!!,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) == PackageManager.PERMISSION_GRANTED
                ) {
                    //Request location updates:
                    googleMap!!.isMyLocationEnabled = true
                }
            }


            markerPoints = ArrayList()

            googleMap!!.uiSettings.isCompassEnabled = true
            googleMap!!.uiSettings.isMyLocationButtonEnabled = true
            googleMap!!.uiSettings.isRotateGesturesEnabled = true


            LoadMarkers()

            val mgr = activity!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            val network_enabled = mgr.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
            val location: Location?


            if (network_enabled) {
                if (checkLocationPermission()) {
                    location =
                        mgr.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)// i permis concedit et pilla ultima ubicacio


                    if (location != null) {
                        val cameraPosition =
                            CameraPosition.Builder().target(LatLng(location.latitude, location.longitude)).zoom(12f)
                                .build()
                        googleMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
                    }


                    mgr.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1f, object : LocationListener {
                        override fun onLocationChanged(location: Location) {

                        }

                        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}

                        override fun onProviderEnabled(provider: String) {}

                        override fun onProviderDisabled(provider: String) {}

                    })


                }
            }
        }

        return rootView


    }


    fun LoadMarkers() {
        showLoading!!.visibility = View.VISIBLE

        Type_Ads = "Adopciones"
        val rootRef = FirebaseDatabase.getInstance().reference
        val perdidosUbiRef = rootRef.child(Type_Ads)
        val eventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                var ubicacion: LatLng//ubicacion va canviant a mesurea recorrent base de dades
                for (ds in dataSnapshot.children) {
                    val id = ds.key
                    val name = ds.child("name").getValue(String::class.java)
                    val descripcion = ds.child("descripción").getValue(String::class.java)
                    val ImgUrl = ds.child("photoUrl").getValue(String::class.java)
                    val time = ds.child("fecha").getValue(String::class.java)
                    val Lat = ds.child("lat").getValue(Float::class.java)
                    val Long = ds.child("lng").getValue(Float::class.java)
                    ubicacion = LatLng(
                        java.lang.Float.parseFloat(Lat.toString()).toDouble(),
                        java.lang.Float.parseFloat(Long.toString()).toDouble()
                    )
                    var pin: BitmapDescriptor? = null
                    pin = bitmapDescriptorFromVector(activity, R.drawable.ic_pin_adoption)


                    val markers = googleMap!!.addMarker(
                        MarkerOptions()
                            .position(ubicacion)
                            .title(name)
                            .snippet(Type_Ads)
                            .icon(pin)
                    )

                    markers.tag = id
                    extraData[markers] = time!!
                    extraData2[markers] = ImgUrl!!

                    extraData3[markers] = Type_Ads


                    googleMap!!.setOnInfoWindowClickListener { marker ->
                        DataPet(marker.snippet, marker.tag.toString(), 0)
                    }


                }

            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        }
        perdidosUbiRef.addListenerForSingleValueEvent(eventListener)
        showLoading!!.visibility = View.INVISIBLE

        if (googleMap != null)
            googleMap!!.setInfoWindowAdapter(
                MarkerWindow_Adapter(
                    LayoutInflater.from(activity),
                    extraData,
                    extraData2,
                    extraData3
                )
            )

    }

    //llama ficha mascota enviando tipoanuncio y id anuncio.
    private fun DataPet(tipoAnuncio: String, id: String, requestCode: Int) {
        // Log.i("ArrayAds", "DataPet: "+tipoAnuncio+"id: "+id);

        val intent = Intent(activity, DataPetActivity::class.java)//va a edit text
        intent.putExtra("TipoAnuncioFicha", tipoAnuncio)
        intent.putExtra("IdAnuncioFicha", id)
        startActivityForResult(intent, requestCode)
    }


    fun RefreshMap() {
        Type_Ads = "Adopciones"
        //borra los markers
        if (googleMap != null) googleMap!!.clear()
        if (markerPoints != null) markerPoints!!.clear()
        //carga markers
        LoadMarkers()

    }

    override fun onResume() {
        super.onResume()
        mMapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mMapView.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        mMapView.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mMapView.onLowMemory()
    }

    fun checkLocationPermission(): Boolean {
        //if there are not permissions, it asks again
        if (ContextCompat.checkSelfPermission(
                activity!!,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    activity!!,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) {
                //Request o pregunta del permís
                ActivityCompat.requestPermissions(activity!!, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 101)


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(
                    activity!!,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    1
                )
            }
            return false
        } else {
            return true
        }
    }

    companion object {
        var Type_Ads = "Adopciones"
        //message
        var messages: Boolean? = null
    }
}// Required empty public constructor
