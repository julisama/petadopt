package com.ramos.julian.petadopt.fragments.Buscar


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView

import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import com.ramos.julian.petadopt.R
import com.ramos.julian.petadopt.fragments.Buscar.MarkerCallback
import com.squareup.picasso.Picasso

import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.Days
import org.joda.time.Hours
import org.joda.time.Minutes
import org.joda.time.Months
import org.joda.time.Seconds
import org.joda.time.Years

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Date
import java.util.HashMap
import java.util.Locale

class MarkerWindow_Adapter(
    private val inflater: LayoutInflater,
    private val extraData: HashMap<Marker, String>,
    private val extraData2: HashMap<Marker, String>,
    private val extraData3: HashMap<Marker, String>
) : GoogleMap.InfoWindowAdapter {

    private var title: TextView? = null
    private var time: TextView? = null
    private var typeAd: TextView? = null
    private var img: ImageView? = null

    private var typeAdString: String? = null
    private var type_adLayout: RelativeLayout? = null
    //coge los componentes de la consulta en el mapa_fragment
    override fun getInfoWindow(m: Marker): View {
        //Carga layout personalizado.
        val v = inflater.inflate(R.layout.activity_marker_window__adapter, null)
        context = v.getContext()

        title = v.findViewById(R.id.title)
        img = v.findViewById(R.id.img)
        time = v.findViewById(R.id.time)
        typeAd = v.findViewById(R.id.et_typeAd)
        type_adLayout = v.findViewById(R.id.type_adLayout)
        title!!.text = m.title

        typeAdString = extraData3[m]

        typeAd!!.text = "ADOPCIÓN"
        type_adLayout!!.setBackgroundResource(R.drawable.title_rounded_blue)


        //tiempo
        //diferencia fecha anuncio y la actual
        //convierte string data a long
        var date: Date? = null
        var dateLong: Long = 0
        val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
        try {
            date = format.parse(extraData[m])
            dateLong = date!!.time
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val DifTime = getTimeDifference(dateLong)
        time!!.text = DifTime
        //picasso agafa url i ho converteix amb imatge
        Picasso.get()
            .load(extraData2[m])
            .resize(150, 150)
            .into(img!!, MarkerCallback(m))
        // markercallback para que no solo en la segunda vez de clicar se vea la imagen

        return v
    }

    private fun getTimeDifference(date: Long): String {
        val today = DateTime.now()
        val modified = DateTime(date, DateTimeZone.UTC)

        val years = Years.yearsBetween(modified, today).getYears()
        val months = Months.monthsBetween(modified, today).getMonths()
        val days = Days.daysBetween(modified, today).getDays()
        val hours = Hours.hoursBetween(modified, today).getHours()
        val minutes = Minutes.minutesBetween(modified, today).getMinutes()
        val seconds = Seconds.secondsBetween(modified, today).getSeconds()

        return if (years > 1) {
            String.format(Locale.getDefault(), context!!.getString(R.string.years), years)
        } else if (years > 0) {
            String.format(Locale.getDefault(), context!!.getString(R.string.year), years)
        } else if (months > 1) {
            String.format(Locale.getDefault(), context!!.getString(R.string.months), months)
        } else if (months > 0) {
            String.format(Locale.getDefault(), context!!.getString(R.string.month), months)
        } else if (days > 1) {
            String.format(Locale.getDefault(), context!!.getString(R.string.days), days)
        } else if (days > 0) {
            String.format(Locale.getDefault(), context!!.getString(R.string.day), days)
        } else if (hours > 1) {
            String.format(Locale.getDefault(), context!!.getString(R.string.hours), hours)
        } else if (hours > 0) {
            String.format(Locale.getDefault(), context!!.getString(R.string.hour), hours)
        } else if (minutes > 1) {
            String.format(Locale.getDefault(), context!!.getString(R.string.minutes), minutes)
        } else if (minutes > 0) {
            String.format(Locale.getDefault(), context!!.getString(R.string.minute), minutes)
        } else if (seconds > 1) {
            String.format(Locale.getDefault(), context!!.getString(R.string.seconds), seconds)
        } else {
            String.format(Locale.getDefault(), context!!.getString(R.string.second), seconds)
        }
    }

    override fun getInfoContents(m: Marker): View? {
        return null
    }

    companion object {
        private val TAG = "MarkerWindow_Adapter"

        private var context: Context? = null
    }
}
