package com.ramos.julian.petadopt

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_reset_password.*

class ResetPasswordActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // val actionBar = supportActionBar!!.hide()

        setContentView(R.layout.activity_reset_password)
        var email: String? = null
        btn_resPass.setOnClickListener {
            email = et_email.text.toString()
            if (email != null && !email!!.isEmpty()) {
                resetPass(email!!)
            } else {
                Toast.makeText(this, R.string.resetPassEmptyName, Toast.LENGTH_SHORT).show()

            }
        }

    }

    private fun resetPass(email: String) {
        var mAuth: FirebaseAuth? = null
        mAuth = FirebaseAuth.getInstance()

        mAuth!!.sendPasswordResetEmail(email)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Toast.makeText(this, R.string.resetPassSent, Toast.LENGTH_SHORT).show()
                    finish()

                } else {
                    // failed!
                    Toast.makeText(this, R.string.resetPassFailed, Toast.LENGTH_SHORT).show()
                }
            }


    }

}
