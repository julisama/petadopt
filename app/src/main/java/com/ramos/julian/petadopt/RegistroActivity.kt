package com.ramos.julian.petadopt

import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.net.Uri
import android.text.TextUtils
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast

import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.location.places.ui.PlacePicker
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_registro.*

class RegistroActivity : AppCompatActivity() {


    private var auth: FirebaseAuth? = null

    //place api
    private var direccion: String? = null
    internal var Lat = 0f
    internal var Lng = 0f
    internal lateinit var LatLng: com.google.android.gms.maps.model.LatLng


    override fun onCreate(savedInstanceState: Bundle?) {//crear usuario
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_registro)
        //Get Firebase auth instance
        auth = FirebaseAuth.getInstance()


        //place
        btGetPlace.setOnClickListener {
            val builder = PlacePicker.IntentBuilder()
            try {
                val intent = builder.build(this@RegistroActivity)
                startActivityForResult(intent, PLACE_PICKER_REQUEST)
            } catch (e: GooglePlayServicesRepairableException) {
                e.printStackTrace()
            } catch (e: GooglePlayServicesNotAvailableException) {
                e.printStackTrace()
            }
        }
        legal.setOnClickListener { openURLinBrowserContact() }

        btn_registrar.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                val name = re_nom.text.toString().trim { it <= ' ' }
                val email = re_email!!.text.toString().trim { it <= ' ' } //final que no es pot canviar
                val password = re_password.text.toString().trim { it <= ' ' }
                val re_password = re_password2.text.toString().trim { it <= ' ' }

                if (TextUtils.isEmpty(name)) {
                    Toast.makeText(applicationContext, "¡Pon tu nombre!", Toast.LENGTH_SHORT).show()
                    return
                }

                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(applicationContext, "¡Pon tu email!", Toast.LENGTH_SHORT).show()
                    return
                }

                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(applicationContext, "¡Pon tu contraseña!", Toast.LENGTH_SHORT).show()
                    return
                }

                if (password.length < 6) {
                    Toast.makeText(
                        applicationContext,
                        "¡Contraseña demasiado corta, mínimo 6 carácteres!",
                        Toast.LENGTH_SHORT
                    ).show()
                    return
                }

                if (password != re_password) {
                    Toast.makeText(
                        applicationContext,
                        "¡Las contraseñas no coinciden! Por favor introduce el mismo valor",
                        Toast.LENGTH_SHORT
                    ).show()
                    return
                }
                if (TextUtils.isEmpty(direccion)) {
                    Toast.makeText(applicationContext, "Introduce una ubicación por favor", Toast.LENGTH_SHORT).show()
                    return
                }

                if (!adultCheckBox.isChecked) {
                    Toast.makeText(applicationContext, "¡Debes de ser mayor de edad!", Toast.LENGTH_SHORT).show()
                    return
                }

                linearLayout_progressBar.visibility = View.VISIBLE
                //create user
                auth!!.createUserWithEmailAndPassword(
                    email,
                    password
                ) // va a crear el usuari a base dades autentificacio
                    .addOnCompleteListener(
                        this@RegistroActivity
                    ) { task ->
                        //diu si la tasca completa a ono
                        //diu si la tasca completa a ono
                        //Toast.makeText(RegistroActivity.this, "createUserWithEmail:onComplete:" + task.isSuccessful(), Toast.LENGTH_SHORT).show();
                        linearLayout_progressBar.visibility = View.GONE
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful) {//si la tasca falla
                            Toast.makeText(
                                this@RegistroActivity,
                                "Correo usado o inválido, por favor introduzca un correo válido.",
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {//si la tasca registra introduim les dades com el nom i imatge en defaulti ntrem a usuari i acabem
                            val mDatabase = FirebaseDatabase.getInstance().reference.child("Usuarios")


                            val currentUserDB = mDatabase.child(auth!!.currentUser!!.uid)
                            currentUserDB.child("name").setValue(name)
                            currentUserDB.child("email").setValue(email)
                            currentUserDB.child("password").setValue(password)
                            currentUserDB.child("image").setValue("default")
                            currentUserDB.child("messages").setValue(false)

                            //currentUserDB.child("Ads").setValue("default");

                            currentUserDB.child("Settings").child("lat").setValue(Lat)
                            currentUserDB.child("Settings").child("lng").setValue(Lng)
                            currentUserDB.child("Settings").child("alerts").setValue(true)
                            currentUserDB.child("Settings").child("alertPerdidos").setValue(true)
                            currentUserDB.child("Settings").child("alertEncontrados").setValue(true)
                            currentUserDB.child("Settings").child("alertAdopciones").setValue(true)
                            currentUserDB.child("Settings").child("radius").setValue(10)
                            currentUserDB.child("Settings").child("address").setValue(direccion)





                            startActivity(Intent(this@RegistroActivity, MainActivity::class.java))
                            finish()
                        }
                    }

            }
        })
    }

    override fun onResume() {
        super.onResume()
        linearLayout_progressBar.visibility = View.GONE
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        //al retornar intent de la ubicación
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                val place = PlacePicker.getPlace(this, data!!)
                tvPlaceName.text = place.name
                tvPlaceAddress.text = place.address
                direccion = place.address as String
                LatLng = place.latLng
                Lat = LatLng.latitude.toFloat()
                Lng = LatLng.longitude.toFloat()

                //Log.i("placeapi", "long y lat "+Lat+" "+Lng);

            }
        }
    }

    /*
       private void requestPermission() {
           //api place
           if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
               if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                   requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_FINE_LOCATION);
               }
           }
       }
       */
    fun openURLinBrowserContact() {
        val browserIntent =
            Intent(Intent.ACTION_VIEW, Uri.parse("https://www.freeprivacypolicy.com/blog/privacy-policy-mobile-apps/"))
        startActivity(browserIntent)
    }

    companion object {
        private val MY_PERMISSION_FINE_LOCATION = 101
        private val PLACE_PICKER_REQUEST = 1
    }
}