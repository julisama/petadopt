package com.ramos.julian.petadopt.models

class GalleryCard(
    var id: String?,
    var tipoAnuncio: String?,
    var name: String?,
    private var time: String?,
    var km: String?,
    var img: String?
) {

    fun getTime(): String? {
        return time
    }

    fun setTime(km: String) {
        this.time = time
    }
}
