package com.ramos.julian.petadopt

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast

import com.google.firebase.auth.FirebaseAuth
import com.ramos.julian.petadopt.R
import com.ramos.julian.petadopt.models.Pet
import com.ramos.julian.petadopt.utils.GenericFileProvider
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.Circle
import com.google.android.gms.maps.model.CircleOptions
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target

import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Date

import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_data_pet.*

class DataPetActivity : AppCompatActivity(), OnMapReadyCallback {
    private var TipoAnuncio: String? = null
    private var IdAnuncio: String? = null

    //mapa
    private var mMap: GoogleMap? = null

    private var userId: String? = null
    private var img: String? = null
    private var nameAutor: String? = null

    internal var mMapView: MapView? = null
    internal var map: GoogleMap? = null

    //modelo
    internal var FichaPet: Pet? = null
    //share
    private var mTarget: Target? = null
    //telf
    private var telfString: String? = null

    internal lateinit var IdUserActual: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data_pet)


        //coger datos intent
        val intent = intent
        TipoAnuncio = intent.getStringExtra("TipoAnuncioFicha")
        IdAnuncio = intent.getStringExtra("IdAnuncioFicha")

        //chat
        IdUserActual = FirebaseAuth.getInstance().currentUser!!.uid

        imgPet.setOnClickListener { onClickImage() }
        btn_telf.setOnClickListener { CallPhone() }




        DataFirebase()

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)
    }

    private fun CallPhone() {

        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:" + telfString!!)
        startActivity(intent)

    }

    fun DataFirebase() {
        //poner info base de datos firebase
        val rootRef = FirebaseDatabase.getInstance().reference
        val uidRef = rootRef.child(TipoAnuncio!!).child(IdAnuncio!!)
        val eventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                img = dataSnapshot.child("photoUrl").getValue(String::class.java)
                val name = dataSnapshot.child("name").getValue(String::class.java)
                val raza = dataSnapshot.child("raza").getValue(String::class.java)
                val pelo = dataSnapshot.child("pelo").getValue(String::class.java)
                val ojos = dataSnapshot.child("ojos").getValue(String::class.java)
                val edad = dataSnapshot.child("edad").getValue(String::class.java)
                val direccion = dataSnapshot.child("direccion").getValue(String::class.java)
                val fecha = dataSnapshot.child("fecha").getValue(String::class.java)
                val descripcion = dataSnapshot.child("descripción").getValue(String::class.java)
                val lat = dataSnapshot.child("lat").getValue(Float::class.java)
                val lng = dataSnapshot.child("lng").getValue(Float::class.java)
                val sex = dataSnapshot.child("sex").getValue(String::class.java)
                val telf = dataSnapshot.child("telf").getValue(Int::class.javaPrimitiveType)!!
                val collar = dataSnapshot.child("collar").getValue(Boolean::class.javaPrimitiveType)!!
                val chip = dataSnapshot.child("chip").getValue(Boolean::class.javaPrimitiveType)!!
                val type_animal = dataSnapshot.child("type_pet").getValue(String::class.java)

                userId = dataSnapshot.child("userId").getValue(String::class.java)
                if (IdUserActual == userId) {
                    imgProfile!!.visibility = View.GONE
                    title_user!!.visibility = View.GONE
                    userPet!!.visibility = View.GONE
                    btn_telf!!.visibility = View.GONE
                }
                Picasso.get()
                    .load(img)
                    .resize(408, 300)//tamaño imagen, si es mayor la recorta
                    .centerCrop()
                    .into(imgPet)

                FichaPet = Pet(
                    name,
                    descripcion,
                    raza,
                    pelo,
                    ojos,
                    img,
                    userId,
                    lat!!,
                    lng!!,
                    fecha,
                    direccion,
                    sex,
                    collar,
                    chip,
                    telf,
                    type_animal,
                    TipoAnuncio
                )
                centerCamera()//como ya obtiene datos puede coger latlng y seleccionarlo en el mapa.

                //poner la info en variables
                namePet.setText(FichaPet!!.name)
                title_typeAnimal!!.setText(FichaPet?.type_pet)
                razaPet.setText(FichaPet!!.raza)
                peloPet.setText(FichaPet!!.pelo)
                ojosPet.setText(FichaPet!!.ojos)
                if (FichaPet!!.sex.equals("M")) {
                    title_sex!!.text = "MACHO"
                    img_sex.setImageResource(R.drawable.ic_masculine)
                }
                if (FichaPet!!.sex.equals("F")) {
                    title_sex!!.text = "HEMBRA"
                    img_sex.setImageResource(R.drawable.ic_femenine)

                }
                if (FichaPet!!.sex.equals("U")) {
                    title_sex!!.text = "DESCONOCIDO"
                    img_sex.setImageResource(R.drawable.ic_unknown)

                }
                //formato fecha que se visualiza
                val fechaPet = FichaPet!!.fecha
                val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
                var date: Date? = null
                try {
                    date = format.parse(fechaPet)
                } catch (e: ParseException) {
                    e.printStackTrace()
                }

                val dateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss a")
                val datetime = dateFormat.format(date)

                direccionPet.text = "Se adopta en " + FichaPet!!.direccion + " el " + datetime
                et_typeAd.text = "ADOPCIÓN"
                type_adLayout.setBackgroundResource(R.drawable.title_rounded_blue)

                descripcionPet.setText(FichaPet!!.descripción)
                DataUser(userId)
                //call phone
                telfString = (FichaPet!!.telf).toString()
                if (telfString == "0") btn_telf!!.visibility = View.GONE
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        }
        uidRef.addListenerForSingleValueEvent(eventListener)
    }

    private fun DataUser(userID: String?) {
        val rootRef = FirebaseDatabase.getInstance().reference
        val uidRef = rootRef.child("Usuarios").child(userID!!)
        val eventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val nameUser = dataSnapshot.child("name").getValue(String::class.java)
                val imgProfileString = dataSnapshot.child("image").getValue(String::class.java)
                //write to layout
                userPet!!.text = nameUser
                nameAutor = nameUser
                if (imgProfileString != "default" && imgProfileString != null) {
                    //picasso
                    Picasso.get()
                        .load(imgProfileString)
                        .resize(54, 54)
                        .centerCrop()
                        .into(imgProfile)
                }

            }

            override fun onCancelled(databaseError: DatabaseError) {}
        }
        uidRef.addListenerForSingleValueEvent(eventListener)
    }

    //MAPA
    private fun centerCamera() {
        Log.i("locationpet", "${FichaPet!!.lat} ${FichaPet!!.lng}")
        if (FichaPet != null && FichaPet!!.lat != null && FichaPet!!.lng != null && mMap != null) {
            val location = LatLng(
                java.lang.Float.parseFloat((FichaPet!!.lat).toString()).toDouble(),
                java.lang.Float.parseFloat((FichaPet!!.lng).toString()).toDouble()
            )
            mMap!!.moveCamera(CameraUpdateFactory.newLatLng(location))
            mMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 16.0f))
            Log.i("locationpet", location.toString())

            val circleOptions = CircleOptions()
                .center(
                    LatLng(
                        java.lang.Float.parseFloat((FichaPet!!.lat).toString()).toDouble(),
                        java.lang.Float.parseFloat((FichaPet!!.lng).toString()).toDouble()
                    )
                )
                .radius(75.0)
                .strokeWidth(0f)
                .fillColor(Color.argb(35, 0, 0, 0))

            val circle = mMap!!.addCircle(circleOptions)
            circle.center = location
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

    }

    //To show the image full size
    fun onClickImage() {
        val intent = Intent(this@DataPetActivity, ShowImageActivity::class.java)
        intent.putExtra("imagePet", img)
        startActivity(intent)
        overridePendingTransition(R.animator.slide_in_up, R.animator.no_anim)
    }

}

