package com.ramos.julian.petadopt.models

class Pet(
    var name: String?,
    var descripción: String?,
    var raza: String?,
    var pelo: String?,
    var ojos: String?,
    var photoUrl: String?,
    var userId: String?,
    var lat: Float,
    var lng: Float,
    var fecha: String?,
    var direccion: String?,
    var sex: String?,
    var collar: Boolean,
    var chip: Boolean,
    var telf: Int,
    var type_pet: String?,
    var typeAd: String?
)
