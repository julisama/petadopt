package com.ramos.julian.petadopt

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Environment
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.ramos.julian.petadopt.utils.GenericFileProvider

import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import kotlinx.android.synthetic.main.activity_post_ad_publication.*

import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class PostAdPublicationActivity : AppCompatActivity(), View.OnClickListener {
    private var TipoAnuncio: String? = null
    private var IdAnuncio: String? = null
    //share
    private var mTarget: Target? = null

    private var name: String? = null
    private var img2: String? = null
    private var directionDB: String? = null
    private var description: String? = null
    //progressbar
    private var showLoading: LinearLayout? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_ad_publication)
        TipoAnuncio = intent.extras!!.getString("TipoAnuncio")
        IdAnuncio = intent.extras!!.getString("IdAnuncio")

        DataFirebase()
    }

    private fun DataFirebase() {
        val rootRef = FirebaseDatabase.getInstance().reference
        val uidRef = rootRef.child(TipoAnuncio!!).child(IdAnuncio!!)
        val eventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                img2 = dataSnapshot.child("photoUrl").getValue(String::class.java)
                name = dataSnapshot.child("name").getValue(String::class.java)
                directionDB = dataSnapshot.child("direccion").getValue(String::class.java)
                description = dataSnapshot.child("descripción").getValue(String::class.java)

                //picasso agafa url i ho converteix amb imatge
                Picasso.get()
                    .load(img2)
                    .resize(408, 300)//tamaño imagen, si es mayor la recorta
                    .centerCrop()
                    .into(img)

                //poner la info en variables
                Ad_title!!.text = name
                Ad_direction!!.text = directionDB

                et_typeAd!!.text = "ADOPCIÓN"
                type_adLayout!!.setBackgroundResource(R.drawable.title_rounded_blue)


            }

            override fun onCancelled(databaseError: DatabaseError) {}
        }
        uidRef.addListenerForSingleValueEvent(eventListener)


    }


    override fun onClick(v: View) {
        when (v.id) {
            R.id.close_imageView -> onBackPressed()
            R.id.share -> onClickShare()
        }
    }

    override fun onBackPressed() {
        this.finish()
        // overridePendingTransition(R.animator.no_anim, R.animator.slide_out_down);
        finish()
    }

    //To share with image pet
    fun onClickShare() {
        showLoading!!.visibility = View.VISIBLE
        mTarget = object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom) {
                val i = Intent(Intent.ACTION_SEND)
                i.type = "image/*"
                i.putExtra(Intent.EXTRA_STREAM, getLocalBitmapUri(bitmap))
                i.putExtra(
                    android.content.Intent.EXTRA_TEXT,
                    name + "\n" + description + "\n" + "\n" + getString(R.string.pet_share)
                )
                startActivity(Intent.createChooser(i, "Compartir anuncio"))
                showLoading!!.visibility = View.INVISIBLE

            }

            override fun onBitmapFailed(e: Exception, errorDrawable: Drawable) {

            }

            override fun onPrepareLoad(placeHolderDrawable: Drawable) {}
        }

        Picasso.get().load(img2).into(mTarget!!)
    }

    private fun getLocalBitmapUri(bmp: Bitmap): Uri? {
        var bmpUri: Uri? = null
        try {
            val file = File(
                getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                "share_image_" + System.currentTimeMillis() + ".png"
            )
            val out = FileOutputStream(file)
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out)
            out.close()
            // bmpUri = Uri.parse(img);
            bmpUri = GenericFileProvider.getUriForFile(
                applicationContext,
                applicationContext.packageName + ".provider",
                file
            )
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return bmpUri
    }


}

