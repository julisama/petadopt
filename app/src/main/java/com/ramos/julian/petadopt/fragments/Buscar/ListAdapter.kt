package com.ramos.julian.petadopt.fragments.Buscar

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.support.v7.widget.CardView
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView

import com.ramos.julian.petadopt.models.GalleryCard
import com.ramos.julian.petadopt.R
import com.ramos.julian.petadopt.DataPetActivity
import com.squareup.picasso.Picasso

class ListAdapter(private val mContext: Context, private val anunciosList: List<GalleryCard>) :
    RecyclerView.Adapter<ListAdapter.MyViewHolder>() {
    private var idPet: String? = null
    private var TipoAnuncio: String? = null


    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var title: TextView
        var distance: TextView
        var time: TextView
        var thumbnail: ImageView
        var cardView: CardView

        internal val type_ad: TextView
        internal val type_adLayout: RelativeLayout

        init {
            title = view.findViewById(R.id.title) as TextView
            distance = view.findViewById(R.id.distance) as TextView
            time = view.findViewById(R.id.time) as TextView
            thumbnail = view.findViewById(R.id.img) as ImageView
            type_ad = view.findViewById(R.id.et_typeAd) as TextView
            type_adLayout = view.findViewById(R.id.type_adLayout)

            cardView = view.findViewById(R.id.card_view)


        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.activity_anunciocard, parent, false)


        return MyViewHolder(itemView)
    }

    @SuppressLint("ResourceType")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val anuncios = anunciosList[position]
        holder.title.setText(anuncios.name)
        holder.distance.setText(anuncios.km + " km")
        holder.time.text = anuncios.getTime()
        holder.type_ad.text = "ADOPCIÓN"
        holder.type_adLayout.setBackgroundResource(R.drawable.title_rounded_blue)



        holder.cardView.setOnClickListener { v ->
            DataPetIntent(anuncios, v)
        }

        holder.thumbnail.setOnClickListener { v ->
            DataPetIntent(anuncios, v)
        }

        Picasso.get()
            .load(anuncios.img)
            .resize(200, 200)
            .centerCrop()
            .into(holder.thumbnail)
    }

    private fun DataPetIntent(anuncios: GalleryCard, v: View) {
        TipoAnuncio = anuncios.tipoAnuncio
        idPet = anuncios.id
        val intent = Intent(v.context, DataPetActivity::class.java)
        intent.putExtra("TipoAnuncioFicha", TipoAnuncio)
        intent.putExtra("IdAnuncioFicha", idPet)
        mContext.startActivity(intent)
    }
    private fun showPopupMenu(view: View) {
        // inflate menu
        val popup = PopupMenu(mContext, view)
        val inflater = popup.menuInflater
        // inflater.inflate(R.menu.menu_album, popup.getMenu());
        popup.setOnMenuItemClickListener(MyMenuItemClickListener())
        popup.show()
    }

    internal inner class MyMenuItemClickListener : PopupMenu.OnMenuItemClickListener {

        override fun onMenuItemClick(menuItem: MenuItem): Boolean {
            /*
            switch (menuItem.getItemId()) {
                case R.id.action_add_favourite:
                    Toast.makeText(mContext, "Add to favourite", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.action_play_next:
                    Toast.makeText(mContext, "Play next", Toast.LENGTH_SHORT).show();
                    return true;
                default:

            }*/
            return false
        }

    }

    override fun getItemCount(): Int {
        return anunciosList.size
    }

}
