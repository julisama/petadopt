package com.ramos.julian.petadopt.fragments.Buscar

import com.google.android.gms.maps.model.Marker
import com.squareup.picasso.Callback

class MarkerCallback internal constructor(marker: Marker) : Callback {
    internal var marker: Marker? = null

    init {
        this.marker = marker
    }


    override fun onSuccess() {
        if (marker != null && marker!!.isInfoWindowShown) {
            marker!!.hideInfoWindow()
            marker!!.showInfoWindow()
        }
    }

    override fun onError(e: Exception) {

    }
}